<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    //
    public function Bio(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
        $Firstname= $request["firstname"];
        $Lastname= $request["lastname"];
        $Gender= $request["jk"];
        $Nationaly= $request["Nationaly"];
        $LanguageSpoken= $request["Spoken"];
        $Bio= $request["Bio"];
        return view('halaman.home', compact('Firstname', 'Lastname', 'Gender', 'Nationaly', 'LanguageSpoken', 'Bio'));

    }
}
